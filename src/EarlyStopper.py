class EarlyStopper():
    def __init__(self, metric, patience=3, mode="max"):
        self.patience = patience
        self.mode = mode
        self.metric = metric
        self.non_impr_epoch = 0
        if mode == "max":
            self.current_best = float("-inf")
        elif mode == "min":
            self.current_best = float("inf")

    def check_improvement(self, metrics):
        end = False
        save = True
        try:
            if self.mode == "max":
                if metrics[self.metric] < self.current_best:
                    print("The new value of " + self.metric + " score of " + str(metrics[self.metric]) + " is not better then the old value of " +
                          str(self.current_best) + ".")
                    self.non_impr_epoch += 1
                    save = False
                    if self.non_impr_epoch == self.patience:
                        end = True
                else:
                    print("The new value of " + self.metric + " score of " + str(metrics[self.metric]) + " is better then the old value of " +
                          str(self.current_best) + ".")
                    self.non_impr_epoch = 0
                    self.current_best = metrics[self.metric]
            if self.mode == "min":
                if metrics[self.metric] > self.current_best:
                    print("The new value of " + self.metric + " score of " + str(metrics[self.metric]) + " is not better then the old value of " +
                          str(self.current_best) + ".")
                    self.non_impr_epoch += 1
                    save = False
                    if self.non_impr_epoch == self.patience:
                        end = True
                else:
                    print("The new value of " + self.metric + " score of " + str(metrics[self.metric]) + " is better then the old value of " +
                          str(self.current_best) + ".")
                    self.non_impr_epoch = 0
                    self.current_best = metrics[self.metric]
            return (save, end)
        except KeyError:
            raise KeyError("The chosen metric was not assigned.")
