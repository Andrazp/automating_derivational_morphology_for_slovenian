import torch
from torch import nn, optim
from tqdm import tqdm, trange
import numpy as np
import sys
from copy import deepcopy

class TaggerTrainer():

    def __init__(self, model, device, num_labels, ignore_index=255, lr=2e-5, train_epochs=3, weight_decay=0.01, adam_epsilon=1e-8, metrics=None, early_stopping=None, save_path=None):
        self.model = model
        self.device = device
        self.lr = lr
        #self.batch_size = batch_size
        self.weight_decay = weight_decay
        self.train_epochs = train_epochs
        self.adam_epsilon = adam_epsilon
        self.metrics = metrics
        self.early_stopping = early_stopping
        self.save_path = save_path
        self.num_labels = num_labels
        self.ignore_index = ignore_index
    
    def load_weights(self, path):
        self.model.load_state_dict(torch.load(path))

    def save_weights(self, path):
        torch.save(deepcopy(self.model.state_dict()), path)
    
    def train(self, train_dataloader, eval_dataloader=None):

        #loss_function = nn.CrossEntropyLoss(ignore_index=self.ignore_index)
        optimizer = optim.AdamW(self.model.parameters(), lr=self.lr, weight_decay=self.weight_decay, eps=self.adam_epsilon)
        train_iterator = trange(int(self.train_epochs), desc="Epoch")
        self.model.to(self.device)
        tr_loss_track = []
        nr_batches = 0
        for _ in train_iterator:
            self.model.train()
            tr_loss = 0
            for step, batch in enumerate(train_dataloader):
                # Step 1. Remember that Pytorch accumulates gradients.
                # We need to clear them out before each instance
                self.model.train()
                self.model.zero_grad()
                inputs, labels, mask = batch
                #print(inputs.size())
                #print(inputs[0])
                #print(labels[0])
                #print(mask[0])
                inputs = inputs.to(self.device)
                labels = labels.to(self.device)
                mask = mask.to(self.device)
                loss = -self.model(inputs, labels, mask)
                loss.backward()
                optimizer.step()
                #print(outputs.size())
                #print(labels.size())
                
                #outputs = outputs.view(len(outputs[1])*self.batch_size, -1)
                #labels = labels.view(len(outputs[1])*self.batch_size)
                #lets try tu use flatten
                #outputs = torch.flatten(outputs, end_dim=1)
                #labels = torch.flatten(labels)
                
                #print(outputs.size())
                #print(labels.size())
                
                #batch_ce_loss = 0.0
                #for i in range(outputs.size(0)):
                #    ce_loss = loss_function(outputs[i], labels[i])
                #    batch_ce_loss += ce_loss
                
                #loss = loss_function(outputs.view(-1, self.num_labels), labels.view(-1))
                #loss = loss_function(outputs, labels)
                tr_loss += loss.item()
                #batch_ce_loss.backward()
                nr_batches += 1

            if self.early_stopping:
                if not eval_dataloader:
                    raise ValueError("Please provide an evaluation dataset for the early stopping procedure.")
                metrics = self.evaluate(eval_dataloader)
                save, end = self.early_stopping.check_improvement(metrics)
                if save:
                    self.save_weights(self.save_path)
                    #torch.save(self.model.state_dict(), self.save_path)
                if end:
                    break
            #else:
            #    self.save_weights(self.save_path)

            tr_loss = tr_loss / nr_batches
            tr_loss_track.append(tr_loss)

        return tr_loss_track
    
    def evaluate(self, eval_dataloader):
        """Evaluation of trained checkpoint."""
        self.model.to(self.device)
        self.model.eval()
        predictions = []
        true_labels = []
        data_iterator = tqdm(eval_dataloader, desc="Iteration")
        for step, batch in enumerate(data_iterator):
            inputs, labels, mask = batch
            inputs = inputs.to(self.device)
            mask = mask.to(self.device)

            with torch.no_grad():
                outputs = self.model.decode(inputs, mask)

            #outputs = outputs.to('cpu').numpy()
            labels = labels.to('cpu')
            mask = mask.to('cpu')
            for output in outputs:
                #print(len(output))
                for o in output:
                    predictions.append(o)
            
            mask = torch.flatten(mask).numpy()
            labels = torch.flatten(labels).numpy()

            for l, m in zip(labels, mask):
                if m == 1:
                    true_labels.append(l)
            #print(logits)
            #print(label_ids)
            #for label, output in zip(labels, outputs):
            #    true_labels.append(label)
            #    predictions.append(np.argmax(output))

        assert len(predictions) == len(true_labels)
        #print(f"Predictions length {len(predictions)}")
        #print(len(true_labels))
        #print(type(predictions))
        #print(type(true_labels))
        #print(type(predictions[0]))
        #print(type(true_labels[0]))

        metrics = self.get_metrics(predictions, true_labels)
        return metrics
    
    def predict(self, test_dataloader):
        """Inference with trained checkpoint."""
        self.model.to(self.device)
        self.model.eval()
        predictions = []
        data_iterator = tqdm(test_dataloader, desc="Iteration")
        for step, batch in enumerate(data_iterator):
            inputs, labels, mask = batch
            inputs = inputs.to(self.device)
            mask = mask.to(self.device)

            with torch.no_grad():
                outputs = self.model.decode(inputs, mask)

            #outputs = outputs.to('cpu').numpy()
            #labels = labels.to('cpu')
            #mask = mask.to('cpu')
            prediction = []
            for output in outputs:
                #print(len(output))
                for o in output:
                    prediction.append(o)
            predictions.append(prediction)    
            #print(logits)
            #print(label_ids)
            #for label, output in zip(labels, outputs):
            #    true_labels.append(label)
            #    predictions.append(np.argmax(output))

        return predictions
    
    def get_metrics(self, predicted, true):
        metrics = {}
        for k, v in self.metrics.items():
            metrics[k] = v(predicted, true)
        return metrics
