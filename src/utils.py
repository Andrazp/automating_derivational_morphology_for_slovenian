from torch.nn.utils.rnn import pad_sequence

def pad_collate(batch):
  (xx, yy, x_lens) = zip(*batch)
  #x_lens = [len(x) for x in xx]
  #y_lens = [len(y) for y in yy]

  #print(type(x_lens))
  #print(type(y_lens))

  xx_pad = pad_sequence(xx, batch_first=True, padding_value=255)
  yy_pad = pad_sequence(yy, batch_first=True, padding_value=255)

  #return xx_pad, yy_pad, x_lens, y_lens
  return xx_pad, yy_pad, x_lens

def decode(word: str, predictions: list) -> str:
  decoded = ""
  for char, p in zip(word, predictions):
    if p == 0 or p == 2:
      decoded += f"{char}-"
    else:
      decoded += f"{char}"
  return decoded.strip("-")