def get_divisions(predictions, labels):
    divisions_pred = []
    divisions_true = []
    for pred, label in zip(predictions, labels):
        if label == 255:
            continue
        if pred == 0 or pred == 2:
            divisions_pred.append(1)
        #if l == 0:
        #    divisions.append(1)
        #elif l == 2:
        #    if (i+1) < (len(labels) - 1):
        #        if labels[i+1] != 5:
        #            divisions.append(1)
        #        else:
        #            divisions.append(0)
        #    else:
        #        divisions.append(0)
        else:
            divisions_pred.append(0)
        if label == 0 or label == 2:
            divisions_true.append(1)
        else:
            divisions_true.append(0)
    return divisions_pred, divisions_true

def calculate_accuracy(predicted, true):
    predicted_div, true_div = get_divisions(predicted, true)
    true_positive = 0
    for p, t in zip(predicted_div, true_div):
        if p == 1 and p == t:
            true_positive += 1
    
    all = 0
    for p, t in zip(predicted_div, true_div):
        if t == 1:
            all += 1

    acc = true_positive / all
    return acc

def calculate_recall(predicted, true):
    predicted_div, true_div = get_divisions(predicted, true)
    true_positive = 0
    for p, t in zip(predicted_div, true_div):
        if p == 1 and p == t:
            true_positive += 1
    
    false_negative = 0
    for p, t in zip(predicted_div, true_div):
        if p == 0 and t == 1:
            false_negative += 1

    print(true_positive)
    if (true_positive + false_negative) == 0:
        return 0
    else:
        return true_positive/(true_positive + false_negative)

def calculate_precision(predicted, true):
    predicted_div, true_div = get_divisions(predicted, true)
    true_positive = 0
    for p, t in zip(predicted_div, true_div):
        if p == 1 and p == t:
            true_positive += 1
    
    false_positive = 0
    for p, t in zip(predicted_div, true_div):
        if p == 1 and t == 0:
            false_positive += 1
    if (true_positive + false_positive) == 0:
        return 0
    else:
        return true_positive/(true_positive + false_positive)

def calculate_f1(predicted, true):
    r = calculate_recall(predicted, true)
    p = calculate_precision(predicted, true)
    if r+p == 0:
        return 0
    else:
        return 2 * ((r * p) / (r + p))
