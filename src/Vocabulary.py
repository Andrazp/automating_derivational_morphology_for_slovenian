class Vocabulary():

    def __init__(self, words):
        self.vocabulary = self.__create_vocabulary(words)

    def __create_vocabulary(self, words):
        char_to_idx = {}
        for w in words:
            try:
                for char in w:
                    if char not in char_to_idx:
                        char_to_idx[char] = len(char_to_idx) #assign each character a unique index
            except:
                print(w)
        # assign start and stop characters their own index
        #char_to_idx["<w>"] = len(char_to_idx)
        #char_to_idx["<\w>"] = len(char_to_idx)
        char_to_idx["<PAD>"] = len(char_to_idx)
        return char_to_idx
    
    def get_vocabulary_length(self):
        return len(self.vocabulary)

    def get_index(self, char):
        return self.vocabulary[char]