import pandas as pd

def split_data(data_file):
    data = pd.read_csv(data_file)
    for split in range(5):
        data.loc[data["val_split"] != split].to_csv(f"./train_cleaned_{split}.csv", index=False)
        data.loc[data["val_split"] == split].to_csv(f"./test_cleaned_{split}.csv", index=False)

if __name__ == "__main__":
    split_data("../data/transformed_data_cleaned_splits.csv")