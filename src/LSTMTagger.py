import torch
from torch import nn
from torchcrf import CRF

class LSTMTagger(nn.Module):

    def __init__(self, embedding_dim, hidden_dim, vocab_size, tagset_size, batch_size, length, dropout_rate=0.2):
        super(LSTMTagger, self).__init__()
        self.hidden_dim = hidden_dim

        self.char_embeddings = nn.Embedding(vocab_size, embedding_dim)

        self.lstm = nn.LSTM(embedding_dim, hidden_dim, batch_first=True)

        self.dropout = nn.Dropout(p=dropout_rate)

        # The linear layer that maps from hidden state space to tag space
        self.hidden2tag = nn.Linear(hidden_dim, tagset_size)
        self.batch_size = batch_size
        self.length = length

    def forward(self, sentence):
        embeds = self.char_embeddings(sentence)
        #embeds = nn.utils.rnn.pack_padded_sequence(embeds, sentence_lens, batch_first=True, enforce_sorted=False)
        lstm_out, (hidden, cell) = self.lstm(embeds)
        #lstm_out, _ = nn.utils.rnn.pad_packed_sequence(lstm_out, batch_first=True)
        
        #lstm_out, _ = self.lstm(embeds.view(len(sentence), self.batch_size, -1))
        #lstm_out = self.dropout(lstm_out)
        #tag_space = self.hidden2tag(lstm_out.view(len(sentence), self.batch_size, -1))
        tag_space = self.hidden2tag(lstm_out)
        #tag_scores = F.log_softmax(tag_space, dim=1)
        return tag_space


class BiLSTMTagger(nn.Module):

    def __init__(self, embedding_dim, hidden_dim, vocab_size, tagset_size, batch_size, length, dropout_rate=0.2):
        super(BiLSTMTagger, self).__init__()
        self.hidden_dim = hidden_dim

        self.char_embeddings = nn.Embedding(vocab_size, embedding_dim)

        self.lstm = nn.LSTM(embedding_dim, hidden_dim, num_layers=2, bidirectional=True, batch_first=True)

        self.dropout = nn.Dropout(p=dropout_rate)

        # The linear layer that maps from hidden state space to tag space
        self.hidden2tag = nn.Linear(hidden_dim, tagset_size)
        self.batch_size = batch_size
        self.length = length

    def forward(self, sentence):
        embeds = self.char_embeddings(sentence)
        #embeds = nn.utils.rnn.pack_padded_sequence(embeds, sentence_lens, batch_first=True, enforce_sorted=False)
        lstm_out, (hidden, cell) = self.lstm(embeds)
        #lstm_out, _ = nn.utils.rnn.pad_packed_sequence(lstm_out, batch_first=True)
        
        #lstm_out, _ = self.lstm(embeds.view(len(sentence), self.batch_size, -1))
        #lstm_out = self.dropout(lstm_out)
        #tag_space = self.hidden2tag(lstm_out.view(len(sentence), self.batch_size, -1))
        lstm_out = self.dropout(lstm_out)
        tag_space = self.hidden2tag(lstm_out)
        #tag_scores = F.log_softmax(tag_space, dim=1)
        return tag_space
        

class BiLSTMCRFTagger(nn.Module):

    def __init__(self, embedding_dim, hidden_dim, vocab_size, tagset_size, batch_size, length, dropout_rate=0.2):
        super(BiLSTMCRFTagger, self).__init__()
        self.hidden_dim = hidden_dim

        self.char_embeddings = nn.Embedding(vocab_size, embedding_dim)

        self.lstm = nn.LSTM(embedding_dim, hidden_dim, num_layers=2, bidirectional=True, batch_first=True)

        self.dropout = nn.Dropout(p=dropout_rate)

        # The linear layer that maps from hidden state space to tag space
        self.hidden2tag = nn.Linear(2 * hidden_dim, tagset_size)

        # The CRF layer that maps from hidden state space to tag space
        self.crf = CRF(tagset_size, batch_first=True)
        self.batch_size = batch_size
        self.length = length

    def forward(self, sentence, labels, mask):
        embeds = self.char_embeddings(sentence)
        #embeds = nn.utils.rnn.pack_padded_sequence(embeds, sentence_lens, batch_first=True, enforce_sorted=False)
        lstm_out, (hidden, cell) = self.lstm(embeds)
        #lstm_out, _ = nn.utils.rnn.pad_packed_sequence(lstm_out, batch_first=True)
        
        #lstm_out, _ = self.lstm(embeds.view(len(sentence), self.batch_size, -1))
        #lstm_out = self.dropout(lstm_out)
        #tag_space = self.hidden2tag(lstm_out.view(len(sentence), self.batch_size, -1))
        lstm_out = self.dropout(lstm_out)
        tag_space = self.hidden2tag(lstm_out)
        #print(tag_space.size())
        #print(labels.size())
        #print(mask.size())

        loss = self.crf(tag_space, labels, mask=mask, reduction='mean')
        return loss

        #tag_scores = F.log_softmax(tag_space, dim=1)
        #return tag_space
    
    def decode(self, sentence, mask):
        embeds = self.char_embeddings(sentence)
        #embeds = nn.utils.rnn.pack_padded_sequence(embeds, sentence_lens, batch_first=True, enforce_sorted=False)
        lstm_out, (hidden, cell) = self.lstm(embeds)
        #lstm_out, _ = nn.utils.rnn.pad_packed_sequence(lstm_out, batch_first=True)
        
        #lstm_out, _ = self.lstm(embeds.view(len(sentence), self.batch_size, -1))
        #lstm_out = self.dropout(lstm_out)
        #tag_space = self.hidden2tag(lstm_out.view(len(sentence), self.batch_size, -1))
        lstm_out = self.dropout(lstm_out)
        tag_space = self.hidden2tag(lstm_out)

        output = self.crf.decode(tag_space, mask=mask)
        return output
