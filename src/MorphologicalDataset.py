from torch.utils.data import Dataset
import torch

class MorphologicalDataset(Dataset):

    def __init__(self, words, morphemes, vocabulary, length, train=True):
        self.words = words
        self.morphemes = morphemes
        self.vocabulary = vocabulary
        self.length = length
        self.train = train

    def __len__(self):
        return len(self.words)

    def __getitem__(self, item):
        characters = [self.vocabulary.get_index(char) for char in self.words[item]]
        #print(characters)
        #characters.insert(0, self.vocabulary.get_index("<w>"))
        #characters.append(self.vocabulary.get_index("<\w>"))
        x_lens = len(characters)
        while len(characters) < self.length:
            characters.append(self.vocabulary.get_index("<PAD>"))
        if self.train == True:
            morphemes = self.morphemes[item]
            labels = []
            # 0 = single character morph
            # 1 = beggining of the morph
            # 2 = end of the morph
            # 3 = middle of the morph
            morphemes = morphemes.split("-")
            for m in morphemes:
                for char in m:
                    if len(m) == 1:
                        labels.append(0)
                    elif m.index(char) == 0:
                        labels.append(1)
                    elif m.index(char) == (len(m) - 1):
                        labels.append(2)
                    else:
                        labels.append(3)
            # 4 START
            # 5 STOP
            #labels.insert(0, 4)
            #labels.append(5)
            while len(labels) < self.length:
                labels.append(3)
            
            mask_1 = []
            mask_0 = []
            for i in range(self.length):
                if i <= (x_lens - 1):
                    mask_1.append(1)
                else:
                    mask_0.append(0)
            
            assert len(mask_1) == x_lens
            mask = mask_1 + mask_0
            assert len(mask) == self.length
            #print(labels)
            #if len(characters) != len(labels):
            #    print(self.words[item])
            #    print(self.morphemes[item])
            return torch.tensor(characters, dtype=torch.long), torch.tensor(labels, dtype=torch.long), torch.tensor(mask, dtype=torch.uint8)
        else:
            return torch.tensor(characters, dtype=torch.long), torch.tensor(mask, dtype=torch.uint8)
