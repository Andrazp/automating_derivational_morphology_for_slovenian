import pandas as pd
from collections import deque

def list_remove_all(arg_l, x):
    return [l for l in arg_l if l != x]


def transform_data(words, morphemes, m_types, splits, remove_anomalies=False):
    added_words = []
    new_morphemes = []
    new_splits = []
    out_counter = 0
    for word, morph, type, split in zip(words, morphemes, m_types, splits):
        #remove anomalies flag that checks if the anomaly is added to the dataset or not
        r_a = False

        chain = morph.split("→")
        type_chain = type.split("→")
        true_morphemes = [c for c in chain[-1].strip().split("-")]
        if "prefix" in type_chain[-1].split("-")[0]:
            in_position = 1
        else:
            in_position = 0
        
        if "#" in true_morphemes:
            in_position = true_morphemes.index("#") + 1

        for c, t in zip(chain[-2:0:-1], type_chain[-2:0:-1]):
            true_first_morphemes = []
            #print(true_morphemes)
            current_chain = c.split("-")
            current_type_chain = t.split("-")
            first_morpheme = true_morphemes[in_position]
            for cc, ct in zip(current_chain, current_type_chain):
                cc = cc.strip()
                ct = ct.strip()
                #print("CC: " + cc)
                #print("Morpheme: " + first_morpheme)
                #first_morpheme = true_morphemes[0]
                #split_cc = cc.split("-")
                if "prefix" in ct:
                    true_morphemes.insert(0, cc)
                    in_position += 1
                    first_morpheme = first_morpheme[len(cc):]
                    continue
                counter = 0
                morpheme = ""
                for i, sc in enumerate(cc):
                    #print("SC: " + sc)
                    if i < len(first_morpheme):
                        if sc == first_morpheme[i]:
                            morpheme += sc
                            counter += 1
                        else:
                            break
                if morpheme != "":
                    true_first_morphemes.append(morpheme)
                    first_morpheme = first_morpheme[counter:]
            try:
                if first_morpheme:
                    true_first_morphemes[-1] = true_first_morphemes[-1] + first_morpheme
            except:
                print(true_first_morphemes)
                print(morph)
                out_counter += 1
                r_a = True
            if true_first_morphemes:
                _ = true_morphemes.pop(in_position)
                for t in true_first_morphemes[::-1]:
                    true_morphemes.insert(in_position, t)
            #print(true_morphemes)
            #if "prefix" in t:
            #        in_position += 1
        if remove_anomalies:
                if r_a:
                    continue
        true_morphemes = list_remove_all(true_morphemes, '0')
        true_morphemes = list_remove_all(true_morphemes, "#")
        combined_morphemes = ""
        for m in true_morphemes:
            combined_morphemes += m + "-"
        new_morphemes.append(combined_morphemes.rstrip("-"))
        word = word.split(" ")[0]
        added_words.append(word)
        new_splits.append(split)
    print(out_counter)    
    return added_words, new_morphemes, new_splits   


if __name__ == "__main__":
    file = "../data/bssjb_valsplit.tsv"
    data = pd.read_csv(file, sep="\t")
    morphemes = data["Morphemes"]
    words = data["Word"]
    m_types = data["GenTypes"]
    splits = data["ValSplit"]
    print(splits)
    added_words, new_morphemes, new_splits = transform_data(words, morphemes, m_types, splits)
    dict_data = {"word": added_words, "morphemes": new_morphemes, "val_split": new_splits}
    transformed_data = pd.DataFrame(data=dict_data)
    transformed_data.to_csv("./transformed_data_splits.csv", index=False)
    added_words, new_morphemes, new_splits= transform_data(words, morphemes, m_types, splits, remove_anomalies=True)
    dict_data = {"word": added_words, "morphemes": new_morphemes, "val_split": new_splits}
    transformed_data = pd.DataFrame(data=dict_data)
    transformed_data.to_csv("./transformed_data_cleaned_splits.csv", index=False)
    #new_morphemes = transform_data(["bajta → bajt-ar → bajtar-ica → bajtarič-in"], ["root → root-suffix → root-suffix → root-suffix"])
    #print(new_morphemes)
    #new_morphemes = transform_data(["bahati → bah-ač → bahač-ast → pre-bahačast"], ["root → root-suffix → root-suffix → prefix-root"])
    #print(new_morphemes)
    #new_morphemes = transform_data(["bahati → bah-at → baha-#-rit-iti → baharit-en"], ["root → root-suffix → root-in-root-suffix → root-suffix"])
    #print(new_morphemes)
