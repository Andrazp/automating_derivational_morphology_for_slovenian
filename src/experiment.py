#from bert_ml_sentiment_classifier import bert_train, bert_evaluate
from LSTMTagger import LSTMTagger, BiLSTMTagger, BiLSTMCRFTagger
from TaggerTrainer import TaggerTrainer
from MorphologicalDataset import MorphologicalDataset
from metrics import calculate_accuracy, calculate_precision, calculate_recall, calculate_f1
from Vocabulary import Vocabulary
from EarlyStopper import EarlyStopper
from utils import pad_collate, decode

import torch
from torch.utils.data import RandomSampler, SequentialSampler, DataLoader
import pandas as pd
import numpy as np

import random
import argparse
import os
import sys
import math
import json
import subprocess


def run():
    parser = argparse.ArgumentParser()

    parser.add_argument("--train_data_file",
                        required=True,
                        type=str)
    parser.add_argument("--test_data_file",
                        required=True,
                        type=str)
    parser.add_argument("--output_dir",
                        required=True,
                        type=str)
    #parser.add_argument("--predictions_file_path",
    #                    type=str,
    #                    default="./predictions.cv")
    #parser.add_argument("--log_path",
    #                    required=True,
    #                    type=str)

    parser.add_argument("--max_len",
                        default=30,
                        type=int)
    parser.add_argument("--batch_size",
                        default=32,
                        type=int)
    parser.add_argument("--num_epochs",
                        default=50,
                        type=int)

    parser.add_argument("--embedding_size",
                        default=50,
                        type=int)
    parser.add_argument("--hidden_size",
                        default=25,
                        type=int)

    parser.add_argument("--learning_rate",
                        default=2e-5,
                        type=float)
    parser.add_argument("--weight_decay",
                        default=0.01,
                        type=float)
    #parser.add_argument("--warmup_proportion",
    #                    default=0.1,
    #                    type=float)
    parser.add_argument("--adam_epsilon",
                        default=1e-8,
                        type=float)
    parser.add_argument("--random_seed",
                       default=42,
                        type=int)
    
    args = parser.parse_args()

    print("Setting the random seed...")
    random.seed(args.random_seed)
    np.random.seed(args.random_seed)
    torch.manual_seed(args.random_seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    
    print("Loading data...")
    data = pd.read_csv(args.train_data_file)
    len_data = len(data)
    #train_data = data[:math.floor(0.7 * len_data)]
    #eval_data = data[math.floor(0.7 * len_data):math.floor(0.9 * len_data)]
    #est_data = data[math.floor(0.9 * len_data):]
    train_data = data[:math.floor(0.9 * len_data)]
    eval_data = data[math.floor(0.9 * len_data):]
    test_data = pd.read_csv(args.test_data_file)
    
    vocab = Vocabulary(train_data["word"])
    train_dataset = MorphologicalDataset(train_data["word"].tolist(), train_data["morphemes"].tolist(), vocab, args.max_len)
    train_dataloader = DataLoader(train_dataset, sampler=RandomSampler(train_dataset), batch_size=args.batch_size)
    eval_dataset = MorphologicalDataset(eval_data["word"].tolist(), eval_data["morphemes"].tolist(), vocab, args.max_len)
    eval_dataloader = DataLoader(eval_dataset, sampler=RandomSampler(eval_dataset), batch_size=args.batch_size)
    test_dataset = MorphologicalDataset(test_data["word"].tolist(), test_data["morphemes"].tolist(), vocab, args.max_len)
    test_dataloader = DataLoader(test_dataset, sampler=SequentialSampler(test_dataset), batch_size=args.batch_size)
    
    print("Training...")
    model_path = os.path.join(args.output_dir, "model.bin")
    log_path = os.path.join(args.output_dir, "log.txt")
    
    metrics = {"accuracy": calculate_accuracy, "precision": calculate_precision, "recall": calculate_recall, "f1": calculate_f1}
    #model = LSTMTagger(args.embedding_size, args.hidden_size, vocab.get_vocabulary_length(), 6, args.batch_size, 30)
    model = BiLSTMCRFTagger(args.embedding_size, args.hidden_size, vocab.get_vocabulary_length(), 4, args.batch_size, 30)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    trainer = TaggerTrainer(model, device, 4, lr=args.learning_rate, train_epochs=args.num_epochs, 
                            weight_decay=args.weight_decay, adam_epsilon=args.adam_epsilon, metrics=metrics, 
                            early_stopping=EarlyStopper("f1", patience=50), save_path=model_path)
    _ = trainer.train(train_dataloader, eval_dataloader)
    print(_)
    metrics = trainer.evaluate(test_dataloader)
    with open(log_path, 'w') as f:
        for k, v in metrics.items():
            print("{}: {}".format(str(k), str(v)))
            f.write("{}: {}\n".format(str(k), str(v)))
            
    print("INFERENCE")
    predictions_path = os.path.join(args.output_dir, "predictions.csv")
    test_dataloader = DataLoader(test_dataset, sampler=SequentialSampler(test_dataset), batch_size=1)
    outputs = trainer.predict(test_dataloader)
    #print(outputs)

    decoded = []
    for word, output in zip(test_data["word"].tolist(), outputs):
        decoded.append(decode(word, output))
    
    test_data["predicted"] = decoded
    test_data.to_csv(predictions_path)
    print("Done.")


if __name__ == "__main__":
    run()