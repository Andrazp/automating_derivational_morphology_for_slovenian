def get_divisions(labels):
    divisions = []
    for i, l in enumerate(labels):
        if l == 0 or l == 2:
            divisions.append(i)
    return divisions

def recall(predicted, true):
    d_predicted = []
    for p in predicted:
        d_predicted.append(get_divisions(p))
    d_true = []
    for t in true:
        d_true.append(get_divisions(t))
    true_positive = 0
    bottom = 0
    for pred, t in zip(d_predicted, d_true):
        for p in pred:
            if p in t:
                true_positive += 1
        bottom += len(t)
    return true_positive/bottom

def precision(predicted, true):
    d_predicted = []
    for p in predicted:
        d_predicted.append(get_divisions(p))
    d_true = []
    for t in true:
        d_true.append(get_divisions(t))
    true_positive = 0
    bottom = 0
    for pred, t in zip(d_predicted, d_true):
        for p in pred:
            if p in t:
                true_positive += 1
        bottom += len(p)
    return true_positive/bottom

def f1(recall, precision):
    return 2 * ((recall * precision) / (recall + precision))