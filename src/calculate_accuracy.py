import pandas as pd
import os

def calculate_accuracy():
    dir_path = "../../data/predictions_18_4"
    files = os.listdir(dir_path)
    acc_per_fold = []
    all = 0
    true = 0
    for file in files:
        file_path = os.path.join(dir_path, file)
        data = pd.read_csv(file_path)
        print(len(data))
        print(len(data.loc[data['morphemes'] == data['predicted']]))
        all += len(data)
        true += len(data.loc[data['morphemes'] == data['predicted']])
        acc_per_fold.append(len(data.loc[data['morphemes'] == data['predicted']])/len(data))
    print(true/all)
    print(acc_per_fold)

if __name__ == "__main__":
    calculate_accuracy()