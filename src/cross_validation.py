#from bert_ml_sentiment_classifier import bert_train, bert_evaluate
from LSTMTagger import LSTMTagger, BiLSTMTagger
from TaggerTrainer import TaggerTrainer
from MorphologicalDataset import MorphologicalDataset
from metrics import calculate_precision, calculate_recall, calculate_f1
from Vocabulary import Vocabulary
from EarlyStopper import EarlyStopper
from utils import pad_collate

import torch
from torch.utils.data import RandomSampler, SequentialSampler, DataLoader
import pandas as pd
import numpy as np

import random
import argparse
import os
import sys
import math
import json
import subprocess


def run():
    parser = argparse.ArgumentParser()

    parser.add_argument("--train_data_file",
                        required=True,
                        type=str)
    #parser.add_argument("--test_data_file",
    #                    required=True,
    #                    type=str)
    parser.add_argument("--output_dir",
                        required=True,
                        type=str)
    #parser.add_argument("--log_path",
    #                    required=True,
    #                    type=str)

    parser.add_argument("--max_len",
                        default=30,
                        type=int)
    parser.add_argument("--batch_size",
                        default=32,
                        type=int)
    parser.add_argument("--num_epochs",
                        default=50,
                        type=int)

    parser.add_argument("--embedding_size",
                        default=50,
                        type=int)
    parser.add_argument("--hidden_size",
                        default=25,
                        type=int)

    parser.add_argument("--learning_rate",
                        default=2e-5,
                        type=float)
    parser.add_argument("--weight_decay",
                        default=0.01,
                        type=float)
    #parser.add_argument("--warmup_proportion",
    #                    default=0.1,
    #                    type=float)
    parser.add_argument("--adam_epsilon",
                        default=1e-8,
                        type=float)
    parser.add_argument("--random_seed",
                       default=42,
                        type=int)
    
    args = parser.parse_args()

    print("Setting the random seed...")
    random.seed(args.random_seed)
    np.random.seed(args.random_seed)
    torch.manual_seed(args.random_seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    
    track_recall = []
    track_precision = []
    track_f1 = []

    for i in range(10):
        print("Training model on the split number " + str(i) + "...")
        output_subdir = os.path.join(args.output_dir, str(i))
        if not os.path.exists(output_subdir):
            os.mkdir(output_subdir)
        model_file = os.path.join(output_subdir, "model.pt")


        print("Loading data...")
        data = pd.read_csv(args.train_data_file)

        test_data = data[(math.floor(len(data) * i * 0.1)):(math.floor(len(data) * (i + 1) * 0.1))]
        rest_data_1 = data[:math.floor((len(data) * i * 0.1))]
        rest_data_2 = data[math.floor((len(data) * (i + 1) * 0.1)):]
        rest_data = pd.concat([rest_data_1, rest_data_2], ignore_index=True)
        print(type(rest_data))

        train_data = rest_data[:math.floor(0.8 * len(rest_data))]
        eval_data = rest_data[math.floor(0.8 * len(rest_data)):]
        print(type(train_data))

        vocab = Vocabulary(train_data["word"])
        train_dataset = MorphologicalDataset(train_data["word"].tolist(), train_data["morphemes"].tolist(), vocab)
        train_dataloader = DataLoader(train_dataset, sampler=RandomSampler(train_dataset), batch_size=args.batch_size, collate_fn=pad_collate)
        eval_dataset = MorphologicalDataset(eval_data["word"].tolist(), eval_data["morphemes"].tolist(), vocab)
        eval_dataloader = DataLoader(eval_dataset, sampler=RandomSampler(eval_dataset), batch_size=args.batch_size, collate_fn=pad_collate)
        test_dataset = MorphologicalDataset(test_data["word"].tolist(), test_data["morphemes"].tolist(), vocab)
        test_dataloader = DataLoader(test_dataset, sampler=SequentialSampler(test_dataset), batch_size=args.batch_size, collate_fn=pad_collate)
        
        print("Training...")
        metrics = {"precision": calculate_precision, "recall": calculate_recall, "f1": calculate_f1}
        #model = LSTMTagger(args.embedding_size, args.hidden_size, vocab.get_vocabulary_length(), 6, args.batch_size, 30)
        model = BiLSTMTagger(args.embedding_size, args.hidden_size, vocab.get_vocabulary_length(), 6, args.batch_size, 30)
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        trainer = TaggerTrainer(model, device, 6, batch_size=args.batch_size, lr=args.learning_rate, train_epochs=args.num_epochs, 
                                weight_decay=args.weight_decay, adam_epsilon=args.adam_epsilon, metrics=metrics, 
                                early_stopping=EarlyStopper("f1", patience=5), save_path=model_file)
        _ = trainer.train(train_dataloader, eval_dataloader)
        print(_)
        metrics = trainer.evaluate(test_dataloader)
        track_precision.append(metrics["precision"])
        track_recall.append(metrics["recall"])
        track_f1.append(metrics["f1"])
    
    avg_precision = np.mean(track_precision)
    avg_recall = np.mean(track_recall)
    avg_f1 = np.mean(track_f1)
    std_precision = np.std(track_precision)
    std_recall = np.std(track_recall)
    std_f1 = np.std(track_f1)

    print("Track recall:")
    print(track_recall)
    print("Track precision:")
    print(track_precision)
    print("Track f1:")
    print(track_f1)
    
    print("Recall: {} +- {}".format(avg_recall, std_recall))
    print("Precision: {} +- {}".format(avg_precision, std_precision))
    print("F1: {} +- {}".format(avg_f1, std_f1))


if __name__ == "__main__":
    run()